import static org.junit.Assert.*;
import ku.util.Stack;
import ku.util.StackFactory;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
/**
 * Stack test JUnit for test case of stackfactory that correct or not.
 * @author Phasin Sarunpornkul
 *
 */

public class StackTest {

	private Stack stack;
	
	@Before
	public void setUp( ) {
		StackFactory.setStackType( 1 ); 
		stack =  StackFactory.makeStack( 5 );
	}
	
	@Test
	public void newStackIsEmpty() {
	assertTrue( stack.isEmpty() );
	assertFalse( stack.isFull() );
	assertEquals( 0, stack.size() );
	}
	
	@Test( expected=java.util.EmptyStackException.class )
	public void testPopEmptyStack() {
	Assume.assumeTrue( stack.isEmpty() );
	stack.pop();
	fail("Pop empty stack should throw exception");
	}
	
	@Test
	public void testStackCapacity(){
		assertEquals( stack.capacity(),5);
	}
	
	@Test 
	public void testFullStack(){
		stack.push('a');
		stack.push('a');
		stack.push('a');
		stack.push('a');
		stack.push('a');
		assertTrue(stack.isFull());
	}
	
	@Test
	public void testPushStack(){
		stack.push('a');
		stack.push('b');
	}
	
	@Test(expected=IllegalArgumentException.class )
	public void testNullPush(){
		stack.push(null);
	}
	
	@Test
	public void testPeekStack(){
		stack.push('a');
		stack.push('b');
		assertEquals(stack.peek(),'b');
	}
	
	@Test
	public void testPeekEmptyStack(){
		assertNull(stack.peek());
	}
	
	@Test
	public void testPopStack(){
		stack.push('a');
		stack.push('b');
		stack.push('c');
		assertEquals('c',stack.pop());
		assertEquals('b',stack.pop());
		assertEquals('a',stack.pop());
		assertTrue(stack.isEmpty());
	}
	
	@Test
	public void testSizeStack(){
		stack.push('a');
		stack.push('a');
		assertEquals(2,stack.size());
	}
	
	@Test(expected=IllegalStateException.class )
	public void testPushFullStack(){
		stack.push('a');
		stack.push('a');
		stack.push('a');
		stack.push('a');
		stack.push('a');
		stack.push('a');
	}
}
